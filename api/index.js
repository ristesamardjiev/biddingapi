import Express from 'express'
import cors from 'cors'

const app = Express()
app.use(cors({
    origin: '*'
}))
app.use(Express.json())

const PORT = 3000

// POST
app.post("/bid", (req, res) => {
    res.setHeader('Access-Control-Allow-Credentials', true)
    res.setHeader('Access-Control-Allow-Origin', '*')
    // another common pattern
    // res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
    res.setHeader('Access-Control-Allow-Methods', 'GET,OPTIONS,PATCH,DELETE,POST,PUT')
    res.setHeader(
        'Access-Control-Allow-Headers',
        'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version'
    )
    if (req.method === 'OPTIONS') {
        res.status(200).end()
        return
    }
    const { amount } = req.body

    if (!amount || amount === '0') res.status(500).send('Missing "amount" parameter!')

    const MIN = +amount + 10;
    const MAX = +amount + 100;

    const isBidding = new Date().valueOf() % 2 === 0
    const bidAmount = isBidding ? (Math.floor(Math.random() * (MAX - MIN + 1)) + MIN) : undefined

    res.json({ bidAmount, isBidding })
})

app.listen(process.env.PORT, () => console.log('Listening on port: ' + PORT))

module.exports = app;
